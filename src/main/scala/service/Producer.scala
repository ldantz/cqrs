package service
import org.apache.kafka.clients.producer.{KafkaProducer, ProducerRecord}
import java.util.Properties
object Producer extends App {



  def SendStringMessage(msg: String) : Unit = {
    val BROKER_LIST = "35.198.127.67:9092"
    val TOPIC = "states"
    val props = new Properties()
    props.put("bootstrap.servers", BROKER_LIST)
    props.put("client.id", "KafkaProducer")
    props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer")
    props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer")

    val producer = new KafkaProducer[String, String](props)
    val data = new ProducerRecord[String, String](TOPIC, msg)
    producer.send(data)

  }
}