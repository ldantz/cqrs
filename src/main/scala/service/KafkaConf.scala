package service

trait KafkaConf {
  lazy val kafkaHost = "35.198.127.67"
  lazy val kafkaPort = "9092"
  lazy val kafkaTopic = "states"
}
