package service
import java.util.{Collections, Properties}

import org.apache.kafka.clients.consumer.{ConsumerRecord, ConsumerRecords, KafkaConsumer}
import com.mongodb.casbah.Imports._
import org.mongodb.scala.{Completed, Observable, Observer}

import scala.collection.JavaConversions._
import repository.Common._
import repository.Database
object Consumer {

  def readMessage(): Unit ={
    val consumerProperties = new Properties()
    consumerProperties.put("bootstrap.servers", "35.198.127.67:9092")
    consumerProperties.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer")
    consumerProperties.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer")
    consumerProperties.put("group.id", "KafkaConsumer")
    val consumer = new KafkaConsumer[String, String](consumerProperties)
    consumer.subscribe(Collections.singletonList("states"))

    while(true) {

      // READING
      val records = consumer.poll(100)

      for (record <- records.iterator()) {
        println(s"Here's your $record")
        val mongoObj = buildMongoDbObject(null)
        //val mongoObj = buildMongoDbObject(record.value())
        val insertObservable: Observable[Completed] = Database.collection.insertOne(mongoObj)
        insertObservable.subscribe(new Observer[Completed] {
          override def onNext(result: Completed): Unit = println(s"onNext: $result")
          override def onError(e: Throwable): Unit = println(s"onError: $e")
          override def onComplete(): Unit = println("onComplete")
        })
        println(Database.collection.find().toString())
      }
    }

    consumer.close()
  }

}
