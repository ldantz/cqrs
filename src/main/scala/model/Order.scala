package model

import java.util.{Date, UUID}

class Order(val orderDate: Date, var paid: Boolean, var sent: Boolean, var articles: scala.collection.mutable.Map[Article,Int] = scala.collection.mutable.Map[Article,Int](), val id: UUID = UUID.randomUUID()){

}

object Orders{

  var Instance: List[Order] = List[Order]()

}