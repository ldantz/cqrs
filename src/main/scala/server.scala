import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.server.Directives._
import akka.stream.ActorMaterializer
import service.{Consumer, Producer}
import spray.json._
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import command.{CommandHandler, CreateOrder, UpdateOrder}
import model.Order

import scala.concurrent.ExecutionContext

object Server extends App {
  val host = "0.0.0.0"
  val port = 9000

  implicit val system: ActorSystem = ActorSystem()
  implicit val executor: ExecutionContext = system.dispatcher
  implicit val materializer: ActorMaterializer = ActorMaterializer()

  def route = path("order") {

    get {
      Producer.SendStringMessage("TEST1");
      Consumer.readMessage();
      complete("TEST1")
    } ~
    post{
      entity(as[JsValue]){
        json =>
          val event = CommandHandler.handle(new UpdateOrder(json))
          complete(event.toString.parseJson)
      }
    } ~
    put{
      val event = CommandHandler.handle(new CreateOrder())
      complete(event.toString.parseJson)
    }
  }

  Http().bindAndHandle(route, host, port)
}