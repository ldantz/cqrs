package repository
import com.mongodb.casbah.Imports._
import command.IEvent
import org.mongodb.scala.bson.collection.immutable.Document

object Common {
  /**
    * Convert a Stock object into a BSON format that MongoDb can store.
    */
  def buildMongoDbObject(event: IEvent): Document = {
    val doc: Document = Document("id" -> event.id.toString(),"date" -> event.date.toString(), "event" -> getClass().getSimpleName())
    return doc
  }
}
