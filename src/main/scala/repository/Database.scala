package repository


import org.mongodb.scala
import org.mongodb.scala.MongoClient

object Database {

  val mongoClient: MongoClient = MongoClient("mongodb://35.198.127.67:27017")
  val database: scala.MongoDatabase = mongoClient.getDatabase("cqrs")
  val collection: scala.MongoCollection[scala.Document] = database.getCollection("states")
}
