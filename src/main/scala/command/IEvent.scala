package command

import java.util.{Date, UUID}

trait IEvent {

  val id: UUID
  val date: Date

  override def toString: String = {
    "{\"id\" : \"%s\", \"event\": \"%s\"}".format(id, getClass().getSimpleName())
  }

}
