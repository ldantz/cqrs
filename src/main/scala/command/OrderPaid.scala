package command

import java.util.{Calendar, Date, UUID}

class OrderPaid(val id: UUID, val date: Date = Calendar.getInstance().getTime()) extends IEvent {

}
