package command

import org.mongodb.scala.{Completed, Observable, Observer}
import repository.Common.buildMongoDbObject
import repository.Database
import service.Producer

object CommandHandler extends ICommandHandler {

  override def handle(command: ICommand): IEvent = {
    val event = command.execute()
    Producer.SendStringMessage(event.toString())

    val mongoObj = buildMongoDbObject(event)
    val insertObservable: Observable[Completed] = Database.collection.insertOne(mongoObj)
    insertObservable.subscribe(new Observer[Completed] {
      override def onNext(result: Completed): Unit = println(s"onNext: $result")
      override def onError(e: Throwable): Unit = println(s"onError: $e")
      override def onComplete(): Unit = println("onComplete")
    })

    return event
  }

}
