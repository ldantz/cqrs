package command
import java.util.{Calendar, Date, UUID}

class OrderCreated(val id: UUID, val date: Date = Calendar.getInstance().getTime()) extends IEvent {

}
