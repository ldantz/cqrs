package command

trait ICommandHandler {

  def handle(command: ICommand) : IEvent

}
