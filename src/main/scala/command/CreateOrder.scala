package command

import java.util.Calendar

import model.{Order, Orders}

class CreateOrder extends ICommand {

  override def execute(): IEvent = {
    val order = new Order(Calendar.getInstance().getTime(), false, false)
    Orders.Instance = order :: Orders.Instance

    new OrderCreated(order.id)
  }

}
