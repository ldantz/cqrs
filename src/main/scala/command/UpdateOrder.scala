package command

import java.util.UUID

import model.{Article, Order, Orders}
import spray.json.JsValue

class UpdateOrder(val json: JsValue) extends ICommand {
  override def execute(): IEvent = {
    var event = new OrderNotFound().asInstanceOf[IEvent]
    val id = json.asJsObject.fields("id").toString().replace("\"", "")
    val uuid = UUID.fromString(id)
    val optionOrder = Orders.Instance.find(o => o.id.equals(uuid))
    val orderAny = optionOrder.getOrElse(None)
    if(orderAny != None)
    {
      val order = orderAny.asInstanceOf[Order]
      if(json.asJsObject.fields.contains("articleName")){
        val articleName = json.asJsObject.fields("articleName").toString().replace("\"", "")
        val articleQuantity = json.asJsObject.fields("articleQuantity").toString().toInt
        val article = new Article(articleName, 6)
        order.articles.put(article, articleQuantity)
        event = new OrderValidated(order.id)
      }
      else if(json.asJsObject.fields.contains("paid")){
        order.paid = true
        event = new OrderPaid(order.id)
      }
      else if(json.asJsObject.fields.contains("sent"))
      {
        order.sent = true
        event = new OrderSent(order.id)
      }
    }
    return event
  }
}
