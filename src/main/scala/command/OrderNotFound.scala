package command
import java.util.{Date, UUID}

class OrderNotFound extends IEvent {
  override val id: UUID = null
  override val date: Date = null
}
