package command

trait ICommand {

  def execute() : IEvent

}
