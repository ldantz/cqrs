package command

import java.util.{Calendar, Date, UUID}

class OrderSent(val id: UUID, val date: Date = Calendar.getInstance().getTime()) extends IEvent {

}
