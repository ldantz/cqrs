name := "CQRS2"

version := "1.0.0-SNA¨SHOT"

scalaVersion := "2.11.9"

resolvers ++= Seq("Typesafe Repository" at "http://repo.typesafe.com/typesafe/releases/",
  Resolver.bintrayRepo("hseeberger", "maven"))

lazy val akkaVersion = "2.5.13"
lazy val akkaHttpVersion = "10.1.3"
lazy val kafkaVersion = "1.0.0"
lazy val kafkaStreamsScalaVersion = "0.1.0"
lazy val mongodbVersion = "2.6.0"

libraryDependencies ++= {
  Seq(
    "com.typesafe.akka" %% "akka-actor" % akkaVersion,
    "com.typesafe.akka" %% "akka-stream" % akkaVersion,
    "com.typesafe.akka" %% "akka-http" % akkaHttpVersion,
    "com.typesafe.akka" %% "akka-http-spray-json" % akkaHttpVersion,
    "org.apache.kafka" % "kafka-clients" % kafkaVersion,
    "org.apache.kafka" % "kafka-streams" % kafkaVersion,
    "com.lightbend" %% "kafka-streams-scala" % kafkaStreamsScalaVersion,
    "org.mongodb.scala" %% "mongo-scala-driver" % mongodbVersion,
    "org.mongodb" %% "casbah" % "3.1.1",
    

  )
}

mainClass in (Compile, run) := Some("Server")
